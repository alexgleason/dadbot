const matrixSdk = require('matrix-js-sdk')

/*******************************************************************************
* Matrix connect
*******************************************************************************/

const dadbotUserId = '@dadbot:matrix.org'

var matrixClient = matrixSdk.createClient({
    baseUrl: 'https://matrix.org',
    accessToken: process.env['MATRIX_ACCESS_TOKEN'],
    userId: dadbotUserId
})

/*******************************************************************************
* Core functions
*******************************************************************************/

// Messages any Matrix room
function send(roomId, message) {
  // 2 second delay in messages so it shows DadBot "typing"
  const typingTime = 2000
  matrixClient.sendTyping(roomId, true, typingTime, function() {
    setTimeout(function() {
      matrixClient.sendMessage(roomId, {
        "msgtype": "m.text",
        "body": message
      })
      matrixClient.sendTyping(roomId, false)
    }, typingTime)
  })
}

// Joins any Matrix room
function joinRoom(roomId) {
  matrixClient.joinRoom(roomId).done(function() {
    send(roomId, "Hi, I'm your dad.")
  })
}

// Provides a callback to handle incoming messages
function onMessage(callback) {
  matrixClient.once('sync', function(state, prevState) {
    if (state === 'PREPARED') {
      matrixClient.on("Room.timeline", function(event, room, toStartOfTimeline) {
        if (toStartOfTimeline || event.getSender() === dadbotUserId) {
          return // Ignore DadBot's own messages
        }
        if (event.getType() !== "m.room.message") {
          return // Only respond to normal messages
        }
        callback(room.roomId, event.getContent().body)
      })
    }
  })
}

// Provides a callback to handle room invitations
function onInvited(callback) {
  matrixClient.on("RoomMember.membership", function(event, member) {
    if (member.membership === "invite" && member.userId === dadbotUserId) {
      callback(member.roomId)
    }
  })
}

// Starts DadBot
function start() {
  matrixClient.startClient()
}

/*******************************************************************************
* Export as a module
*******************************************************************************/

module.exports = {
  dadbotUserId: dadbotUserId,
  onMessage: onMessage,
  send: send,
  onInvited: onInvited,
  joinRoom: joinRoom,
  start: start
}
