# DadBot

A Matrix chatbot that makes dad jokes. Add him at `@dadbot:matrix.org` or deploy your own.

## License

DadBot is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
